﻿namespace DistributionConfiguration
{
    partial class ParameterConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnParameterCancel = new System.Windows.Forms.Button();
            this.UploadButton = new System.Windows.Forms.Button();
            this.lblParametersSourceExcel = new System.Windows.Forms.Label();
            this.txtBrowse = new System.Windows.Forms.TextBox();
            this.BrowseButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.chkRevene = new System.Windows.Forms.CheckBox();
            this.chkMarketSe = new System.Windows.Forms.CheckBox();
            this.chkBusiness = new System.Windows.Forms.CheckBox();
            this.chkBillingIns = new System.Windows.Forms.CheckBox();
            this.chkPickupdop = new System.Windows.Forms.CheckBox();
            this.chkAddon = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnParameterCancel
            // 
            this.btnParameterCancel.Location = new System.Drawing.Point(372, 391);
            this.btnParameterCancel.Name = "btnParameterCancel";
            this.btnParameterCancel.Size = new System.Drawing.Size(89, 33);
            this.btnParameterCancel.TabIndex = 20;
            this.btnParameterCancel.Text = "Cancel";
            this.btnParameterCancel.UseVisualStyleBackColor = true;
            this.btnParameterCancel.Click += new System.EventHandler(this.btnParameterCancel_Click);
            // 
            // UploadButton
            // 
            this.UploadButton.Location = new System.Drawing.Point(235, 391);
            this.UploadButton.Name = "UploadButton";
            this.UploadButton.Size = new System.Drawing.Size(97, 33);
            this.UploadButton.TabIndex = 19;
            this.UploadButton.Text = "Upload";
            this.UploadButton.UseVisualStyleBackColor = true;
            this.UploadButton.Click += new System.EventHandler(this.UploadButton_Click);
            // 
            // lblParametersSourceExcel
            // 
            this.lblParametersSourceExcel.AutoSize = true;
            this.lblParametersSourceExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblParametersSourceExcel.Location = new System.Drawing.Point(-247, 179);
            this.lblParametersSourceExcel.Name = "lblParametersSourceExcel";
            this.lblParametersSourceExcel.Size = new System.Drawing.Size(102, 15);
            this.lblParametersSourceExcel.TabIndex = 18;
            this.lblParametersSourceExcel.Text = "Source Excel File";
            // 
            // txtBrowse
            // 
            this.txtBrowse.Location = new System.Drawing.Point(120, 336);
            this.txtBrowse.Multiline = true;
            this.txtBrowse.Name = "txtBrowse";
            this.txtBrowse.Size = new System.Drawing.Size(246, 34);
            this.txtBrowse.TabIndex = 17;
            // 
            // BrowseButton
            // 
            this.BrowseButton.Location = new System.Drawing.Point(372, 337);
            this.BrowseButton.Name = "BrowseButton";
            this.BrowseButton.Size = new System.Drawing.Size(89, 35);
            this.BrowseButton.TabIndex = 16;
            this.BrowseButton.Text = "Browse";
            this.BrowseButton.UseVisualStyleBackColor = true;
            this.BrowseButton.Click += new System.EventHandler(this.BrowseButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBox8);
            this.groupBox3.Controls.Add(this.chkRevene);
            this.groupBox3.Controls.Add(this.chkMarketSe);
            this.groupBox3.Controls.Add(this.chkBusiness);
            this.groupBox3.Controls.Add(this.chkBillingIns);
            this.groupBox3.Controls.Add(this.chkPickupdop);
            this.groupBox3.Controls.Add(this.chkAddon);
            this.groupBox3.Location = new System.Drawing.Point(12, 30);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(449, 278);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Paramaters Configuration";
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(772, 38);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(73, 17);
            this.checkBox8.TabIndex = 6;
            this.checkBox8.Text = "Meal Plan";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // chkRevene
            // 
            this.chkRevene.AutoSize = true;
            this.chkRevene.Location = new System.Drawing.Point(17, 231);
            this.chkRevene.Name = "chkRevene";
            this.chkRevene.Size = new System.Drawing.Size(98, 17);
            this.chkRevene.TabIndex = 5;
            this.chkRevene.Text = "Revenue Code";
            this.chkRevene.UseVisualStyleBackColor = true;
            // 
            // chkMarketSe
            // 
            this.chkMarketSe.AutoSize = true;
            this.chkMarketSe.Location = new System.Drawing.Point(17, 193);
            this.chkMarketSe.Name = "chkMarketSe";
            this.chkMarketSe.Size = new System.Drawing.Size(104, 17);
            this.chkMarketSe.TabIndex = 4;
            this.chkMarketSe.Text = "Market Segment";
            this.chkMarketSe.UseVisualStyleBackColor = true;
            // 
            // chkBusiness
            // 
            this.chkBusiness.AutoSize = true;
            this.chkBusiness.Location = new System.Drawing.Point(17, 154);
            this.chkBusiness.Name = "chkBusiness";
            this.chkBusiness.Size = new System.Drawing.Size(108, 17);
            this.chkBusiness.TabIndex = 3;
            this.chkBusiness.Text = "Business Source ";
            this.chkBusiness.UseVisualStyleBackColor = true;
            // 
            // chkBillingIns
            // 
            this.chkBillingIns.AutoSize = true;
            this.chkBillingIns.Location = new System.Drawing.Point(22, 113);
            this.chkBillingIns.Name = "chkBillingIns";
            this.chkBillingIns.Size = new System.Drawing.Size(105, 17);
            this.chkBillingIns.TabIndex = 2;
            this.chkBillingIns.Text = "Billing Instruction";
            this.chkBillingIns.UseVisualStyleBackColor = true;
            // 
            // chkPickupdop
            // 
            this.chkPickupdop.AutoSize = true;
            this.chkPickupdop.Location = new System.Drawing.Point(22, 76);
            this.chkPickupdop.Name = "chkPickupdop";
            this.chkPickupdop.Size = new System.Drawing.Size(85, 17);
            this.chkPickupdop.TabIndex = 1;
            this.chkPickupdop.Text = "Pickup-Drop";
            this.chkPickupdop.UseVisualStyleBackColor = true;
            // 
            // chkAddon
            // 
            this.chkAddon.AutoSize = true;
            this.chkAddon.Location = new System.Drawing.Point(22, 38);
            this.chkAddon.Name = "chkAddon";
            this.chkAddon.Size = new System.Drawing.Size(62, 17);
            this.chkAddon.TabIndex = 0;
            this.chkAddon.Text = "Add On";
            this.chkAddon.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 337);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 15);
            this.label1.TabIndex = 21;
            this.label1.Text = "Source Excel File";
            // 
            // ParameterConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 433);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnParameterCancel);
            this.Controls.Add(this.UploadButton);
            this.Controls.Add(this.lblParametersSourceExcel);
            this.Controls.Add(this.txtBrowse);
            this.Controls.Add(this.BrowseButton);
            this.Controls.Add(this.groupBox3);
            this.Name = "ParameterConfigForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ParameterConfigForm";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnParameterCancel;
        private System.Windows.Forms.Button UploadButton;
        private System.Windows.Forms.Label lblParametersSourceExcel;
        private System.Windows.Forms.TextBox txtBrowse;
        private System.Windows.Forms.Button BrowseButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox chkRevene;
        private System.Windows.Forms.CheckBox chkMarketSe;
        private System.Windows.Forms.CheckBox chkBusiness;
        private System.Windows.Forms.CheckBox chkBillingIns;
        private System.Windows.Forms.CheckBox chkPickupdop;
        private System.Windows.Forms.CheckBox chkAddon;
        private System.Windows.Forms.Label label1;
    }
}