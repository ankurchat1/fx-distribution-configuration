﻿namespace DistributionConfiguration
{
    partial class ChannalConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPMSCustCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPropertyId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAPIUserNm = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAPIPassword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAuthKey = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAPIUrl = new System.Windows.Forms.TextBox();
            this.btnChannalSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cmbIFSCCode = new System.Windows.Forms.ComboBox();
            this.cmbMessageType = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // txtPMSCustCode
            // 
            this.txtPMSCustCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPMSCustCode.Location = new System.Drawing.Point(207, 28);
            this.txtPMSCustCode.Name = "txtPMSCustCode";
            this.txtPMSCustCode.Size = new System.Drawing.Size(271, 21);
            this.txtPMSCustCode.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "PMS Code";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Property ID";
            // 
            // txtPropertyId
            // 
            this.txtPropertyId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPropertyId.Location = new System.Drawing.Point(207, 65);
            this.txtPropertyId.Name = "txtPropertyId";
            this.txtPropertyId.Size = new System.Drawing.Size(271, 21);
            this.txtPropertyId.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(22, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Interface Code";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(22, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "Message Type";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(22, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 15);
            this.label5.TabIndex = 10;
            this.label5.Text = "API User Name";
            // 
            // txtAPIUserNm
            // 
            this.txtAPIUserNm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAPIUserNm.Location = new System.Drawing.Point(207, 189);
            this.txtAPIUserNm.Name = "txtAPIUserNm";
            this.txtAPIUserNm.Size = new System.Drawing.Size(271, 21);
            this.txtAPIUserNm.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(22, 236);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 15);
            this.label6.TabIndex = 12;
            this.label6.Text = "API Password";
            // 
            // txtAPIPassword
            // 
            this.txtAPIPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAPIPassword.Location = new System.Drawing.Point(207, 233);
            this.txtAPIPassword.Name = "txtAPIPassword";
            this.txtAPIPassword.Size = new System.Drawing.Size(271, 21);
            this.txtAPIPassword.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(22, 277);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "Authorization Key";
            // 
            // txtAuthKey
            // 
            this.txtAuthKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAuthKey.Location = new System.Drawing.Point(207, 274);
            this.txtAuthKey.Name = "txtAuthKey";
            this.txtAuthKey.Size = new System.Drawing.Size(271, 21);
            this.txtAuthKey.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(22, 320);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 15);
            this.label8.TabIndex = 16;
            this.label8.Text = "API Url";
            // 
            // txtAPIUrl
            // 
            this.txtAPIUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAPIUrl.Location = new System.Drawing.Point(207, 317);
            this.txtAPIUrl.Name = "txtAPIUrl";
            this.txtAPIUrl.Size = new System.Drawing.Size(271, 21);
            this.txtAPIUrl.TabIndex = 15;
            // 
            // btnChannalSave
            // 
            this.btnChannalSave.Location = new System.Drawing.Point(207, 376);
            this.btnChannalSave.Name = "btnChannalSave";
            this.btnChannalSave.Size = new System.Drawing.Size(100, 35);
            this.btnChannalSave.TabIndex = 17;
            this.btnChannalSave.Text = "Save";
            this.btnChannalSave.UseVisualStyleBackColor = true;
            this.btnChannalSave.Click += new System.EventHandler(this.btnChannalSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(341, 376);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 35);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cmbIFSCCode
            // 
            this.cmbIFSCCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbIFSCCode.FormattingEnabled = true;
            this.cmbIFSCCode.Location = new System.Drawing.Point(207, 103);
            this.cmbIFSCCode.Name = "cmbIFSCCode";
            this.cmbIFSCCode.Size = new System.Drawing.Size(272, 23);
            this.cmbIFSCCode.TabIndex = 48;
            // 
            // cmbMessageType
            // 
            this.cmbMessageType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMessageType.FormattingEnabled = true;
            this.cmbMessageType.Location = new System.Drawing.Point(206, 147);
            this.cmbMessageType.Name = "cmbMessageType";
            this.cmbMessageType.Size = new System.Drawing.Size(272, 23);
            this.cmbMessageType.TabIndex = 49;
            // 
            // ChannalConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 433);
            this.Controls.Add(this.cmbMessageType);
            this.Controls.Add(this.cmbIFSCCode);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnChannalSave);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtAPIUrl);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtAuthKey);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtAPIPassword);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAPIUserNm);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPropertyId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPMSCustCode);
            this.Name = "ChannalConfigForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ChannalConfigForm";
            this.Load += new System.EventHandler(this.ChannalConfigForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPMSCustCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPropertyId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAPIUserNm;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAPIPassword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAuthKey;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtAPIUrl;
        private System.Windows.Forms.Button btnChannalSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cmbIFSCCode;
        private System.Windows.Forms.ComboBox cmbMessageType;
    }
}