﻿using DistributionConfiguration.BusinessObject.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DistributionConfiguration
{
    public partial class InterfaceConfigForm : Form
    {
        public static string _dbConnection = string.Empty;
        public static string FullPathDB = string.Empty;
        public InterfaceConfigForm()
        {
            InitializeComponent();
        }

        public InterfaceConfigForm(string _connecttion)
        {
            _dbConnection = _connecttion;
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;
            DistributionConfiguration obj1 = new DistributionConfiguration(FullPathDB);
            obj1.Show();
            this.Close();
        }

        private void btnChannalSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPmsCustCode.Text == string.Empty)
                {
                    MessageBox.Show("Please enter PMS Customer Code !!");
                    return;
                }
                else if (txtPropertyId.Text == string.Empty)
                {
                    MessageBox.Show("Please enter Property ID !!");
                    return;
                }
                else if (cmbIFSCCode.SelectedIndex == 0)
                {
                    MessageBox.Show("Please Select the Channel Type !!");
                    return;
                }
                else if (cmbMessageType.SelectedIndex == 0)
                {
                    MessageBox.Show("Please Select the Interface Message Type !!");
                    return;
                }
                else if (cmbSourceInterface.SelectedIndex == 0)
                {
                    MessageBox.Show("Please Select the Source Interface Type !!");
                    return;
                }
                else if (cmbDestinationIntetface.SelectedIndex == 0)
                {
                    MessageBox.Show("Please Select the Destination Interface Type !!");
                    return;
                }
                else if (cmbCommunicationType.SelectedIndex == 0)
                {
                    MessageBox.Show("Please Select the Communication Type !!");
                    return;
                }
                else if (txtAPIUrl.Text == string.Empty)
                {
                    MessageBox.Show("Please enter API Url !!");
                    return;
                }
                else
                {
                    FxDist_InterfaceApiDetails interfaceapi = new FxDist_InterfaceApiDetails();

                    interfaceapi.Pmscustcode = Int64.Parse(txtPmsCustCode.Text.ToString());
                    interfaceapi.PropertyId = Int64.Parse(txtPropertyId.Text.ToString());
                    interfaceapi.IFSCCode = cmbIFSCCode.SelectedValue.ToString();
                    interfaceapi.ApiUsedFor = cmbMessageType.SelectedValue.ToString();
                    interfaceapi.SourceInterface = cmbSourceInterface.SelectedValue.ToString();
                    interfaceapi.DestinationInterface = cmbDestinationIntetface.SelectedValue.ToString();
                    interfaceapi.CommunicationType = cmbCommunicationType.SelectedValue.ToString();
                    interfaceapi.ApiUserName = txtAPIUserName.Text.ToString();
                    interfaceapi.ApiPassword = txtAPIPassword.Text.ToString();
                    interfaceapi.APIAuthenticationKey = txtAuthKey.Text.ToString();
                    interfaceapi.APIUrl = txtAPIUrl.Text.ToString();
                    interfaceapi.ModifyDatetime = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
                    interfaceapi.UserId = "interface@idsnext.com";
                    interfaceapi.IsActive = true;


                    using (SqlConnection con = new SqlConnection(_dbConnection))
                    {
                        string query = "insert into FxDist_InterfaceApiDetailsTestQA values (@Pmscustcode,@PropertyId,@SourceInterface," +
                            "@DestinationInterface,@ApiUsedFor,@CommunicationType" +
                            ",@IFSCCode,@ApiUserName,@ApiPassword,@APIAuthenticationKey," +
                            "@APIUrl,@ModifiedDateTime,@UserId,@IsActive)";
                        using (SqlCommand cmd = new SqlCommand(query))
                        {
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@Pmscustcode", interfaceapi.Pmscustcode);
                            cmd.Parameters.AddWithValue("@PropertyId", interfaceapi.PropertyId);
                            cmd.Parameters.AddWithValue("@SourceInterface", interfaceapi.SourceInterface);
                            cmd.Parameters.AddWithValue("@DestinationInterface", interfaceapi.DestinationInterface);
                            cmd.Parameters.AddWithValue("@ApiUsedFor", interfaceapi.ApiUsedFor);
                            cmd.Parameters.AddWithValue("@CommunicationType", interfaceapi.CommunicationType);
                            cmd.Parameters.AddWithValue("@IFSCCode", interfaceapi.IFSCCode);
                            cmd.Parameters.AddWithValue("@ApiUserName", interfaceapi.ApiUserName);
                            cmd.Parameters.AddWithValue("@ApiPassword", interfaceapi.ApiPassword);
                            cmd.Parameters.AddWithValue("@APIAuthenticationKey", interfaceapi.APIAuthenticationKey);
                            cmd.Parameters.AddWithValue("@APIUrl", interfaceapi.APIUrl);
                            cmd.Parameters.AddWithValue("@ModifiedDateTime", interfaceapi.ModifyDatetime);
                            cmd.Parameters.AddWithValue("@UserId", interfaceapi.UserId);
                            cmd.Parameters.AddWithValue("@IsActive", interfaceapi.IsActive);

                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    MessageBox.Show("Interface Configuration Saved !! ");
                    this.Hide();
                }
            }
            catch (Exception)
            {

                throw;
            }
            
            
        }

        private void InterfaceConfigForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            cmbIFSCCode.DataSource = Enum.GetValues(typeof(InterfaceCode));
            cmbMessageType.DataSource = Enum.GetValues(typeof(ApiUsedFor));
            cmbSourceInterface.DataSource = Enum.GetValues(typeof(DestinationInterface));
            cmbDestinationIntetface.DataSource = Enum.GetValues(typeof(SourceInterface));
            cmbCommunicationType.DataSource = Enum.GetValues(typeof(CommunicationType));

        }

    }
}
