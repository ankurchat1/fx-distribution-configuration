﻿using DistributionConfiguration.BusinessObject;
using DistributionConfiguration.BusinessObject.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DistributionConfiguration
{
    public partial class DistributionConfiguration : Form
    {
        public static string _dbConnection = string.Empty;
        private static string Excel03ConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=YES'";
        public static string FullPathDB = string.Empty;

        public DistributionConfiguration()
        {
            InitializeComponent();
        }

        public DistributionConfiguration(string _connecttion)
        {
            _dbConnection = _connecttion;
            InitializeComponent();
        }
        public static string _filePath { get; set; }
        public static string _targetSQLDb { get; set; }

        private void btnChannalConfig_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;
            ChannalConfigForm obj1 = new ChannalConfigForm(FullPathDB);
            obj1.Show();
            this.Hide();
        }

        private void btnInterfaceConfig_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;
            InterfaceConfigForm obj1 = new InterfaceConfigForm(FullPathDB);
            obj1.Show();
            this.Hide();
        }

        private void btnParameterConfig_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;
            ParameterConfigForm obj1 = new ParameterConfigForm(FullPathDB);
            obj1.Show();
            this.Hide();
        }

        private void btnFXCRSConfig_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;
            FxCRSConfig obj1 = new FxCRSConfig(FullPathDB);
            obj1.Show();
            this.Hide();
        }

        private void FxOnePropertyConfig_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;
            FxOnePropertyConfig obj1 = new FxOnePropertyConfig(FullPathDB);
            obj1.Show();
            this.Hide();
        }
    }
}
