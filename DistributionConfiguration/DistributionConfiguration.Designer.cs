﻿namespace DistributionConfiguration
{
    partial class DistributionConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnChannalConfig = new System.Windows.Forms.Button();
            this.btnInterfaceConfig = new System.Windows.Forms.Button();
            this.btnParameterConfig = new System.Windows.Forms.Button();
            this.btnFXCRSConfig = new System.Windows.Forms.Button();
            this.FxOnePropertyConfig = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnChannalConfig
            // 
            this.btnChannalConfig.Font = new System.Drawing.Font("Sitka Small", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChannalConfig.Location = new System.Drawing.Point(12, 12);
            this.btnChannalConfig.Name = "btnChannalConfig";
            this.btnChannalConfig.Size = new System.Drawing.Size(155, 86);
            this.btnChannalConfig.TabIndex = 0;
            this.btnChannalConfig.Text = "Channal Configuration";
            this.btnChannalConfig.UseVisualStyleBackColor = true;
            this.btnChannalConfig.Click += new System.EventHandler(this.btnChannalConfig_Click);
            // 
            // btnInterfaceConfig
            // 
            this.btnInterfaceConfig.Font = new System.Drawing.Font("Sitka Small", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInterfaceConfig.Location = new System.Drawing.Point(12, 104);
            this.btnInterfaceConfig.Name = "btnInterfaceConfig";
            this.btnInterfaceConfig.Size = new System.Drawing.Size(155, 86);
            this.btnInterfaceConfig.TabIndex = 15;
            this.btnInterfaceConfig.Text = "Interface Configuration";
            this.btnInterfaceConfig.UseVisualStyleBackColor = true;
            this.btnInterfaceConfig.Click += new System.EventHandler(this.btnInterfaceConfig_Click);
            // 
            // btnParameterConfig
            // 
            this.btnParameterConfig.Font = new System.Drawing.Font("Sitka Small", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnParameterConfig.Location = new System.Drawing.Point(12, 196);
            this.btnParameterConfig.Name = "btnParameterConfig";
            this.btnParameterConfig.Size = new System.Drawing.Size(155, 86);
            this.btnParameterConfig.TabIndex = 16;
            this.btnParameterConfig.Text = "Parameters Configuration";
            this.btnParameterConfig.UseVisualStyleBackColor = true;
            this.btnParameterConfig.Click += new System.EventHandler(this.btnParameterConfig_Click);
            // 
            // btnFXCRSConfig
            // 
            this.btnFXCRSConfig.Font = new System.Drawing.Font("Sitka Small", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFXCRSConfig.Location = new System.Drawing.Point(12, 288);
            this.btnFXCRSConfig.Name = "btnFXCRSConfig";
            this.btnFXCRSConfig.Size = new System.Drawing.Size(155, 86);
            this.btnFXCRSConfig.TabIndex = 17;
            this.btnFXCRSConfig.Text = "FX-CRS Configuration";
            this.btnFXCRSConfig.UseVisualStyleBackColor = true;
            this.btnFXCRSConfig.Click += new System.EventHandler(this.btnFXCRSConfig_Click);
            // 
            // FxOnePropertyConfig
            // 
            this.FxOnePropertyConfig.Font = new System.Drawing.Font("Sitka Small", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FxOnePropertyConfig.Location = new System.Drawing.Point(12, 380);
            this.FxOnePropertyConfig.Name = "FxOnePropertyConfig";
            this.FxOnePropertyConfig.Size = new System.Drawing.Size(155, 86);
            this.FxOnePropertyConfig.TabIndex = 18;
            this.FxOnePropertyConfig.Text = "FX-One Property Configuration";
            this.FxOnePropertyConfig.UseVisualStyleBackColor = true;
            this.FxOnePropertyConfig.Click += new System.EventHandler(this.FxOnePropertyConfig_Click);
            // 
            // DistributionConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 478);
            this.Controls.Add(this.FxOnePropertyConfig);
            this.Controls.Add(this.btnFXCRSConfig);
            this.Controls.Add(this.btnParameterConfig);
            this.Controls.Add(this.btnInterfaceConfig);
            this.Controls.Add(this.btnChannalConfig);
            this.Name = "DistributionConfiguration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Distribution Configuration";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnChannalConfig;
        private System.Windows.Forms.Button btnInterfaceConfig;
        private System.Windows.Forms.Button btnParameterConfig;
        private System.Windows.Forms.Button btnFXCRSConfig;
        private System.Windows.Forms.Button FxOnePropertyConfig;
    }
}