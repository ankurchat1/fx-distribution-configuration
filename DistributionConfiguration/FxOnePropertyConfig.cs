﻿using DistributionConfiguration.BusinessObject.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DistributionConfiguration
{
    public partial class FxOnePropertyConfig : Form
    {
        public static string _dbConnection = string.Empty;
        public static string FullPathDB = string.Empty;
        public FxOnePropertyConfig()
        {
            InitializeComponent();
        }

        public FxOnePropertyConfig(string _connecttion)
        {
            _dbConnection = _connecttion;
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;
            DistributionConfiguration obj1 = new DistributionConfiguration(FullPathDB);
            obj1.Show();
            this.Close();

        }

        private void FxOnePropertyConfig_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            cmbSavePriority.DataSource = Enum.GetValues(typeof(SavePrority));
            

        }

        private void btnChannalSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPmsCustCode.Text == string.Empty)
                {
                    MessageBox.Show("Please enter PMS Customer Code !!");
                    return;
                }
                else if (txtPropertyId.Text == string.Empty)
                {
                    MessageBox.Show("Please enter Property ID !!");
                    return;
                }
                else if (txtGroupCode.Text == string.Empty)
                {
                    MessageBox.Show("Please enter Group Code !!");
                    return;
                }
                else if (txtProductCode.Text == string.Empty)
                {
                    MessageBox.Show("Please enter Product Code  !!");
                    return;
                }
                else if (cmbSavePriority.SelectedIndex == 0)
                {
                    MessageBox.Show("Please Select the Save Priority !!");
                    return;
                }
                else
                {
                    FxOnePropertySetting _fx1PropertyMapper = new FxOnePropertySetting();

                    _fx1PropertyMapper.PmsCustCode = Int64.Parse(txtPmsCustCode.Text.ToString());
                    _fx1PropertyMapper.PropertyID = Int64.Parse(txtPropertyId.Text.ToString());
                    _fx1PropertyMapper.GroupCode = Int64.Parse(txtGroupCode.Text.ToString());
                    _fx1PropertyMapper.ProductCode = Int64.Parse(txtProductCode.Text.ToString());
                    _fx1PropertyMapper.SavePriority = cmbSavePriority.SelectedValue.ToString();
                    if (cmbSavePriority.SelectedValue.ToString() == "FOM")
                    { 
                        _fx1PropertyMapper.IsFXFOMEnabled = true;
                       _fx1PropertyMapper.IsFXCRSEnabled = false;

                    }
                    if (cmbSavePriority.SelectedValue.ToString() == "FXCRS")
                    { 
                        _fx1PropertyMapper.IsFXFOMEnabled = false;
                        _fx1PropertyMapper.IsFXCRSEnabled = true;
                    }

                    _fx1PropertyMapper.ModifyDatetime = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
                    _fx1PropertyMapper.UserID = "interface@idsnext.com";
                    

                    using (SqlConnection con = new SqlConnection(_dbConnection))
                    {
                        string query = "insert into FxDist_FxOnePropertyDetails values (@PropertyID,@PmsCustCode,@GroupCode,@ProductCode,@IsFXCRSEnabled,@IsFXFOMEnabled,@SavePriority,@UserId,@ModifiedDateTime)";
                        using (SqlCommand cmd = new SqlCommand(query))
                        {
                            cmd.Connection = con;                            
                            cmd.Parameters.AddWithValue("@PropertyID", _fx1PropertyMapper.PropertyID);
                            cmd.Parameters.AddWithValue("@PmsCustCode", _fx1PropertyMapper.PmsCustCode);
                            cmd.Parameters.AddWithValue("@GroupCode", _fx1PropertyMapper.GroupCode);
                            cmd.Parameters.AddWithValue("@ProductCode", _fx1PropertyMapper.ProductCode);
                            cmd.Parameters.AddWithValue("@IsFXCRSEnabled", _fx1PropertyMapper.IsFXCRSEnabled);
                            cmd.Parameters.AddWithValue("@IsFXFOMEnabled", _fx1PropertyMapper.IsFXFOMEnabled);
                            cmd.Parameters.AddWithValue("@SavePriority", _fx1PropertyMapper.SavePriority);
                            cmd.Parameters.AddWithValue("@UserId", _fx1PropertyMapper.UserID);
                            cmd.Parameters.AddWithValue("@ModifiedDateTime", _fx1PropertyMapper.ModifyDatetime);                           
                            

                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    MessageBox.Show("FX1 Property Configuration Saved !! ");
                    this.Hide();
                }




            }
            catch (Exception ex)
            {
                ex.ToString();

                //throw;
            }
        }
    }
}
