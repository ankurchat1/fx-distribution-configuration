﻿using DistributionConfiguration.BusinessObject.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DistributionConfiguration
{
    public partial class ChannalConfigForm : Form
    {
        public static string _dbConnection = string.Empty;
        public static string FullPathDB = string.Empty;
        public ChannalConfigForm()
        {
            InitializeComponent();
        }

        public ChannalConfigForm(string _connecttion)
        {
            _dbConnection = _connecttion;
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;
            DistributionConfiguration obj1 = new DistributionConfiguration(FullPathDB);
            obj1.Show();
            this.Close();
        }

        private void btnChannalSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPMSCustCode.Text == string.Empty)
                {
                    MessageBox.Show("Please enter PMS Customer Code !!");
                    return;
                }
                else if (txtPropertyId.Text == string.Empty)
                {
                    MessageBox.Show("Please enter Property ID !!");
                    return;
                }                
                else if (cmbIFSCCode.SelectedIndex == 0)
                {
                    MessageBox.Show("Please Select the Channel Type !!");
                    return;
                }
                else if (cmbMessageType.SelectedIndex == 0)
                {
                    MessageBox.Show("Please Select the Interface Message Type !!");
                    return;
                }
                else if (txtAPIUrl.Text == string.Empty)
                {
                    MessageBox.Show("Please enter API Url !!");
                    return;
                }
                else
                {
                    FxDist_ChannelManagerApiDetails _channalMger = new FxDist_ChannelManagerApiDetails();

                    _channalMger.Pmscustcode = Int64.Parse(txtPMSCustCode.Text.ToString());
                    _channalMger.PropertyId = Int64.Parse(txtPropertyId.Text.ToString());
                    _channalMger.IFSCCode = cmbIFSCCode.SelectedValue.ToString();
                    _channalMger.ApiUsedFor = cmbMessageType.SelectedValue.ToString();
                    _channalMger.ApiUserName = txtAPIUserNm.Text.ToString();
                    _channalMger.ApiPassword = txtAPIPassword.Text.ToString();
                    _channalMger.APIAuthenticationKey = txtAuthKey.Text.ToString();
                    _channalMger.APIUrl = txtAPIUrl.Text.ToString();
                    _channalMger.ModifyDatetime = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
                    _channalMger.UserId = "interface@idsnext.com";
                    _channalMger.IsActive = true;

                    using (SqlConnection con = new SqlConnection(_dbConnection))
                    {
                        string query = "insert into FxDist_ChannelManagerApiDetailsTestQA values (@Pmscustcode,@PropertyId,@IFSCCode,@ApiUsedFor,@ApiUserName,@ApiPassword,@APIAuthenticationKey,@APIUrl,@ModifiedDateTime,@UserId,@IsActive)";
                        using (SqlCommand cmd = new SqlCommand(query))
                        {
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@Pmscustcode", _channalMger.Pmscustcode);
                            cmd.Parameters.AddWithValue("@PropertyId", _channalMger.PropertyId);
                            cmd.Parameters.AddWithValue("@IFSCCode", _channalMger.IFSCCode);
                            cmd.Parameters.AddWithValue("@ApiUsedFor", _channalMger.ApiUsedFor);
                            cmd.Parameters.AddWithValue("@ApiUserName", _channalMger.ApiUserName);
                            cmd.Parameters.AddWithValue("@ApiPassword", _channalMger.ApiPassword);
                            cmd.Parameters.AddWithValue("@APIAuthenticationKey", _channalMger.APIAuthenticationKey);
                            cmd.Parameters.AddWithValue("@APIUrl", _channalMger.APIUrl);
                            cmd.Parameters.AddWithValue("@ModifiedDateTime", _channalMger.ModifyDatetime);
                            cmd.Parameters.AddWithValue("@UserId", _channalMger.UserId);
                            cmd.Parameters.AddWithValue("@IsActive", _channalMger.IsActive);

                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    MessageBox.Show("Channel Configuration Saved !! ");
                    this.Hide();
                }



                
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void ChannalConfigForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            cmbIFSCCode.DataSource = Enum.GetValues(typeof(InterfaceCode));
            cmbMessageType.DataSource = Enum.GetValues(typeof(ApiUsedFor));            

        }
    }
}
