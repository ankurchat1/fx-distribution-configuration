﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributionConfiguration.BusinessObject.Model
{
    public class DistributionAPiUrl
    {
        public string MessageType { get; set; }
        public string APIUrl { get; set; }
    }


    public enum ParameterType
    {
        AddOn,
        BillingInstruction,
        MarketSegment,
        BusinessSource,
        PaymentMode,
        RevenueCode,
        MealPlan
    }

    public enum CommunicationType {
        SELECT,
        IN,
        OUT
    }

    public enum ApiUsedFor
    {
        SELECT,
        Reservation,
        HotelPosition,
        Revenue,
        Rate,
        Company,
        Guest,
        GroupBlock
    }

    public enum InterfaceCode {
        SELECT,
        DATAHUB,
        FXFD,
        FXCRS
    }

    public enum SourceInterface
    {
        SELECT,
        DATAHUB,
        FXFD,
        FXCRS
    }

    public enum DestinationInterface
    {
        SELECT,
        DATAHUB,
        FXFD,
        FXCRS
    }

    public enum SavePrority
    {
        SELECT,
        FXCRS,
        FOM

    }


}
