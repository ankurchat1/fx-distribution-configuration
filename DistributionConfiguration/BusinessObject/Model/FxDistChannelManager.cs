﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributionConfiguration.BusinessObject.Model
{
        public class FxDist_ChannelManagerApiDetails  
        {  
            public int ID { get; set; }
            public long Pmscustcode { get; set; }
            public long PropertyId { get; set; }
            public string IFSCCode { get; set; }
            public string ApiUsedFor { get; set; }
            public string ApiUserName { get; set; }
            public string ApiPassword { get; set; }
            public string APIAuthenticationKey { get; set; }
            public string APIUrl { get; set; }
            public DateTime ModifyDatetime { get; set; }
            public string UserId { get; set; }
            public bool? IsActive { get; set; }
        }

    public class FxDist_InterfaceApiDetails  
    {  
        public int ID { get; set; }
        public long Pmscustcode { get; set; }
        public long PropertyId { get; set; }
        public string SourceInterface { get; set; }
        public string DestinationInterface { get; set; }
        public string ApiUsedFor { get; set; }
        public string CommunicationType { get; set; }
        public string IFSCCode { get; set; }
        public string ApiUserName { get; set; }
        public string ApiPassword { get; set; }
        public string APIAuthenticationKey { get; set; }
        public string APIUrl { get; set; }
        public DateTime? ModifyDatetime { get; set; }
        public string UserId { get; set; }
        public bool? IsActive { get; set; }
    }

    public class FxDist_AddonMapping  
    {  
       // public int ID { get; set; }
        public long Pmscustcode { get; set; }
        public long PropertyId { get; set; }
        public string IFSCCode { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Thirdpartycode { get; set; }
        public string ThirdpartycodeDesc { get; set; }
        public DateTime ModifyDateTime { get; set; }
        public string UserId { get; set; }
        public bool? IsDefault { get; set; }
        public bool? IsActive { get; set; }
    }

    public class FxDist_PickupDropMapping
    {
        public int ID { get; set; }

        public long Pmscustcode { get; set; }

        public long PropertyId { get; set; }       

        public string IFSCCode { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public string Thirdpartycode { get; set; }

        public string ThirdpartycodeDesc { get; set; }

        public DateTime? ModifyDateTime { get; set; }

        public string UserId { get; set; }

        public bool? IsDefault { get; set; }

        public bool? IsActive { get; set; }

    }

    public class FxDist_BillingInstructionMapping  
    {  
        public int ID { get; set; }
        public long Pmscustcode { get; set; }
        public long PropertyId { get; set; }
        public string IFSCCode { get; set; }
        public string Pmscode { get; set; }
        public string Description { get; set; }
        public string Thirdpartycode { get; set; }
        public string ThirdpartycodeDesc { get; set; }
        public DateTime? ModifyDateTime { get; set; }
        public string UserId { get; set; }
        public bool? IsDefault { get; set; }
        public bool? IsActive { get; set; }
    }

    public class FxDist_MarketSegmentMapping  
    {  
        public int ID { get; set; }
        public long Pmscustcode { get; set; }
        public long PropertyId { get; set; }
        public string ParameterType { get; set; }
        public string IFSCCode { get; set; }
        public string Pmscode { get; set; }
        public string PmscodeDesc { get; set; }
        public string Thirdpartycode { get; set; }
        public string ThirdpartycodeDesc { get; set; }
        public DateTime? ModifyDateTime { get; set; }
        public string UserId { get; set; }
        public bool? IsActive { get; set; }
    }

    public class FxDist_BusinessSourceMapping  
    {  
        public int ID { get; set; }
        public long Pmscustcode { get; set; }
        public long PropertyId { get; set; }
        public string ParameterType { get; set; }
        public string IFSCCode { get; set; }
        public string Pmscode { get; set; }
        public string PmscodeDesc { get; set; }
        public string Thirdpartycode { get; set; }
        public string ThirdpartycodeDesc { get; set; }
        public DateTime? ModifyDateTime { get; set; }
        public string UserId { get; set; }
        public bool? IsActive { get; set; }
    }

    public class FxDist_PaymodeMapping  
    {  
        public int ID { get; set; }
        public long Pmscustcode { get; set; }
        public long PropertyId { get; set; }
        public string ParameterType { get; set; }
        public string IFSCCode { get; set; }
        public string Pmscode { get; set; }
        public string PmscodeDesc { get; set; }
        public string Thirdpartycode { get; set; }
        public string ThirdpartycodeDesc { get; set; }
        public DateTime? ModifyDateTime { get; set; }
        public string UserId { get; set; }
        public bool? IsDefault { get; set; }
        public bool IsActive { get; set; }
    }

    public class FxDist_RevenueCodeMapping  
    {  
        public int ID { get; set; }
        public long Pmscustcode { get; set; }
        public long PropertyId { get; set; }
        public char IFSCCode { get; set; }
        public char Code { get; set; }
        public string Description { get; set; }
        public char Thirdpartycode { get; set; }
        public string ThirdpartycodeDesc { get; set; }
        public DateTime? ModifyDateTime { get; set; }
        public string UserId { get; set; }
        public bool? IsDefault { get; set; }
        public bool? IsActive { get; set; }
    }


    public class FXPMSAddOnMapping
    {
        //public string ID { get; set; }
        public int Pmscustcode { get; set; } = 20007;
        public int PropertyId { get; set; } = 12121;
        public string IFSCCode { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Thirdpartycode { get; set; }
        public string ThirdpartycodeDesc { get; set; }        
    }

    public class FXPMSPickupDropMapping
    {
        //public int ID { get; set; }

        public int Pmscustcode { get; set; } = 20007;
        public int PropertyId { get; set; } = 12121;

        public string IFSCCode { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public string Thirdpartycode { get; set; }

        public string ThirdpartycodeDesc { get; set; }       

    }

    public class FXPMSBillingInstructionMapping
    {
        //public int ID { get; set; }
        public int Pmscustcode { get; set; } = 20007;
        public int PropertyId { get; set; } = 12121;
        public string IFSCCode { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Thirdpartycode { get; set; }
        public string ThirdpartycodeDesc { get; set; }        
    }

    public class FXPMSBusinessSourceMapping
    {
        //public int ID { get; set; }
        public int Pmscustcode { get; set; } = 20007;
        public int PropertyId { get; set; } = 12121;
        public string ParameterType { get; set; }
        public string IFSCCode { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Thirdpartycode { get; set; }
        public string ThirdpartycodeDesc { get; set; }
        
    }

    public class FXPMSMarketSegmentMapping
    {
        //public int ID { get; set; }
        public int Pmscustcode { get; set; } = 20007;
        public int PropertyId { get; set; } = 12121;
        public string ParameterType { get; set; }
        public string IFSCCode { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Thirdpartycode { get; set; }
        public string ThirdpartycodeDesc { get; set; }        
    }


    public class FxCrsMappingSetting
    {
        public int ID { get; set; } = 1;
        public long PmsCustCode { get; set; }

        public long FxCrsId { get; set; }

        public string PropertyCode { get; set; }

        public bool? IsDHInventoryEnable { get; set; }

        public DateTime? ModifyDatetime { get; set; }

        public string UserId { get; set; }

        public bool? IsActive { get; set; }
    }

    public class FxOnePropertySetting
    {
        public string SavePriority { get; set; }
        public bool IsFXFOMEnabled { get; set; }
        public bool IsFXCRSEnabled { get; set; }
        public long PropertyID { get; set; }
        public long PmsCustCode { get; set; }
        public long GroupCode { get; set; }
        public long ProductCode { get; set; }
        public string UserID { get; set; }
        public DateTime? ModifyDatetime { get; set; }
    }
}
