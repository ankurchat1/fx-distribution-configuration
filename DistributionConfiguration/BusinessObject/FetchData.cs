﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributionConfiguration.BusinessObject
{
    public static class FetchData
    {

        private static string Excel03ConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=YES'";
        
        
        public static DataTable RetrieveSourceDataAddon(string _filePath)
        {
            string sourceConnString = string.Format(Excel03ConString, _filePath, "YES");
            DataTable sourceData = new DataTable();
            using (OleDbConnection conn = new OleDbConnection(sourceConnString))
            {
                conn.Open();
                // Get the data from the source table as a SqlDataReader.
                OleDbCommand command = new OleDbCommand(
                                    @"SELECT [ID],[Pmscustcode],[PropertyId],[IFSCCode],[Code],[Description],[Thirdpartycode],[ThirdpartycodeDesc] 
                                     FROM [AddOn$]", conn);
                DataColumn dataColumn = new DataColumn();
                OleDbDataAdapter adapter = new OleDbDataAdapter(command);
                adapter.Fill(sourceData);
                conn.Close();
            }           

            return sourceData;
        }     


        public static DataTable RetrieveSourceDataPickDrop(string _filePath)
        {
            string sourceConnString = string.Format(Excel03ConString, _filePath, "YES");
            DataTable sourceData = new DataTable();
            using (OleDbConnection conn = new OleDbConnection(sourceConnString))
            {
                conn.Open();
                // Get the data from the source table as a SqlDataReader.
                OleDbCommand command = new OleDbCommand(
                                    @"SELECT [ID],[Pmscustcode],[PropertyId],[IFSCCode],[Code],[Description],[Thirdpartycode],[ThirdpartycodeDesc] 
                                     FROM [PickupDrop$]", conn);
                DataColumn dataColumn = new DataColumn();
                OleDbDataAdapter adapter = new OleDbDataAdapter(command);
                adapter.Fill(sourceData);
                conn.Close();
            }

            return sourceData;
        }

        public static DataTable RetrieveSourceDataBillingInstruction(string _filePath)
        {
            string sourceConnString = string.Format(Excel03ConString, _filePath, "YES");
            DataTable sourceData = new DataTable();
            using (OleDbConnection conn = new OleDbConnection(sourceConnString))
            {
                conn.Open();
                // Get the data from the source table as a SqlDataReader.
                OleDbCommand command = new OleDbCommand(
                                    @"SELECT [ID],[Pmscustcode],[PropertyId],[IFSCCode],[Code],[Description],[Thirdpartycode],[ThirdpartycodeDesc] 
                                     FROM [BillingInstruction$]", conn);
                DataColumn dataColumn = new DataColumn();
                OleDbDataAdapter adapter = new OleDbDataAdapter(command);
                adapter.Fill(sourceData);
                conn.Close();
            }

            return sourceData;
        }

        public static DataTable RetrieveSourceDataMarketSegment(string _filePath)
        {
            string sourceConnString = string.Format(Excel03ConString, _filePath, "YES");
            DataTable sourceData = new DataTable();
            using (OleDbConnection conn = new OleDbConnection(sourceConnString))
            {
                conn.Open();
                // Get the data from the source table as a SqlDataReader.
                OleDbCommand command = new OleDbCommand(
                                    @"SELECT [ID],[Pmscustcode],[PropertyId],[IFSCCode],[Code],[Description],[Thirdpartycode],[ThirdpartycodeDesc] 
                                     FROM [MarketSegment$]", conn);
                DataColumn dataColumn = new DataColumn();
                OleDbDataAdapter adapter = new OleDbDataAdapter(command);
                adapter.Fill(sourceData);
                conn.Close();
            }

            return sourceData;
        }

        public static DataTable RetrieveSourceDataBusinessSource(string _filePath)
        {
            string sourceConnString = string.Format(Excel03ConString, _filePath, "YES");
            DataTable sourceData = new DataTable();
            using (OleDbConnection conn = new OleDbConnection(sourceConnString))
            {
                conn.Open();
                // Get the data from the source table as a SqlDataReader.
                OleDbCommand command = new OleDbCommand(
                                    @"SELECT [ID],[Pmscustcode],[PropertyId],[IFSCCode],[Code],[Description],[Thirdpartycode],[ThirdpartycodeDesc] 
                                     FROM [BusinessSource$]", conn);
                DataColumn dataColumn = new DataColumn();
                OleDbDataAdapter adapter = new OleDbDataAdapter(command);
                adapter.Fill(sourceData);
                conn.Close();
            }

            return sourceData;
        }

        public static DataTable RetrieveSourceDataPaymentMode(string _filePath)
        {
            string sourceConnString = string.Format(Excel03ConString, _filePath, "YES");
            DataTable sourceData = new DataTable();
            using (OleDbConnection conn = new OleDbConnection(sourceConnString))
            {
                conn.Open();
                // Get the data from the source table as a SqlDataReader.
                OleDbCommand command = new OleDbCommand(
                                    @"SELECT [ID],[Pmscustcode],[PropertyId],[IFSCCode],[Code],[Description],[Thirdpartycode],[ThirdpartycodeDesc] 
                                     FROM [PaymentMode$]", conn);
                DataColumn dataColumn = new DataColumn();
                OleDbDataAdapter adapter = new OleDbDataAdapter(command);
                adapter.Fill(sourceData);
                conn.Close();
            }

            return sourceData;
        }
    }
}
