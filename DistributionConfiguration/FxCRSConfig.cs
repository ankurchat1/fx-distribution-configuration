﻿using DistributionConfiguration.BusinessObject.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DistributionConfiguration
{
    public partial class FxCRSConfig : Form
    {
        public static string _dbConnection = string.Empty;
        public static string FullPathDB = string.Empty;
        public FxCRSConfig()
        {
            InitializeComponent();
        }

        public FxCRSConfig(string _connecttion)
        {
            _dbConnection = _connecttion;
            InitializeComponent();
        }

        private void btnChannalSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPmsCustCode.Text == string.Empty)
                {
                    MessageBox.Show("Please enter PMS Customer Code !!");
                    return;
                }
                else if (txtPropertyId.Text == string.Empty)
                {
                    MessageBox.Show("Please enter Property ID !!");
                    return;
                }
                else if (txtProductCode.Text == string.Empty)
                {
                    MessageBox.Show("Please enter the Product Code !!");
                    return;
                }               
                else
                {
                    FxCrsMappingSetting _crsMapper = new FxCrsMappingSetting();

                    _crsMapper.PmsCustCode = Int64.Parse(txtPmsCustCode.Text.ToString());
                    _crsMapper.FxCrsId = Int64.Parse(txtPropertyId.Text.ToString());
                    _crsMapper.PropertyCode = txtProductCode.Text.ToString();
                    if (chkIsEnabled.Checked)
                        _crsMapper.IsDHInventoryEnable = true;
                    else
                        _crsMapper.IsDHInventoryEnable = false;
                    _crsMapper.ModifyDatetime = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
                    _crsMapper.UserId = "interface@idsnext.com";
                    _crsMapper.IsActive = true;

                    using (SqlConnection con = new SqlConnection(_dbConnection))
                    {
                        string query = "insert into FxCrsMapping values (@ID,@Pmscustcode,@FxCrsId,@PropertyCode,@IsDHInventoryEnable,@ModifiedDateTime,@UserId,@IsActive)";
                        using (SqlCommand cmd = new SqlCommand(query))
                        {
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@ID", _crsMapper.ID);
                            cmd.Parameters.AddWithValue("@Pmscustcode", _crsMapper.PmsCustCode);
                            cmd.Parameters.AddWithValue("@FxCrsId", _crsMapper.FxCrsId);
                            cmd.Parameters.AddWithValue("@PropertyCode", _crsMapper.PropertyCode);
                            cmd.Parameters.AddWithValue("@IsDHInventoryEnable", _crsMapper.IsDHInventoryEnable);                            
                            cmd.Parameters.AddWithValue("@ModifiedDateTime", _crsMapper.ModifyDatetime);
                            cmd.Parameters.AddWithValue("@UserId", _crsMapper.UserId);
                            cmd.Parameters.AddWithValue("@IsActive", _crsMapper.IsActive);

                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    MessageBox.Show("FX-CRS Configuration Saved !! ");
                    this.Hide();
                }




            }
            catch (Exception ex)
            {
                ex.ToString();

                //throw;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;
            DistributionConfiguration obj1 = new DistributionConfiguration(FullPathDB);
            obj1.Show();
            this.Close();
        }
    }
}
