﻿using DistributionConfiguration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DistributionConfiguration
{
    public partial class SQLConnectionBuilder : Form
    {

        public SQLConnectionBuilder()
        {
            InitializeComponent();
        }

        private string _serverName = string.Empty;
        private string _databaseName = string.Empty;
        private string _userId = string.Empty;
        private string _password = string.Empty;
        public string FullPathDB { get; set; }

        private void btncancel_Click(object sender, EventArgs e)
        {
            SQLConnectionBuilder sqlconnection = new SQLConnectionBuilder();
            this.Close();
        }        

        private void SQLConnectionBuilder_Load(object sender, EventArgs e)
        {
            //comboDBList.DataSource = GetDatabaseList();   

           // GetPMSVersionValue();


        }

        private List<string> GetDatabaseList(string svr, string us, string pws)
        {
            List<string> list = new List<string>();

            // Open connection to the database
            //string conString = ConfigurationManager.AppSettings["DropDownConnection"]; 
            string conString = "Server="+ svr + ";User ID="+ us + ";Password="+ pws + ";Trusted_Connection=False";

            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();

                // Set up a command with the given query and associate
                // this with the current connection.
                using (SqlCommand cmd = new SqlCommand("SELECT name from sys.databases", con))
                {
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            list.Add(dr[0].ToString());
                        }
                    }
                }
            }
            return list;

        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtserver.Text == string.Empty)
                {
                    MessageBox.Show("Please enter the Database Server Name !!");
                    return;
                }
                else if (txtuser.Text == string.Empty)
                {
                    MessageBox.Show("Please enter a User Name !!");
                    return;
                }
                else if (txtpassword.Text == string.Empty)
                {
                    MessageBox.Show("Please enter a User Name !!");
                    return;
                }

                else
                {                   

                    MessageBox.Show("Server Connected !! ");

                    string DBConnString = GetDBStringWithDB(txtserver.Text, txtuser.Text, txtpassword.Text, comboDBList.Text);
                    FullPathDB = DBConnString;
                    DistributionConfiguration obj1 = new DistributionConfiguration(FullPathDB);
                    obj1.Show();
                    this.Hide();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void comboDBList_MouseClick(object sender, MouseEventArgs e)
        {
            
            _serverName = txtserver.Text;
            _userId = txtuser.Text;
            _password = txtpassword.Text;

            comboDBList.DataSource = GetDatabaseList(_serverName, _userId, _password);


        }       

        

        private string GetDBString(string svr, string us, string pws)
        {
            
            return "Server=" + svr + ";User ID=" + us + ";Password=" + pws + ";Trusted_Connection=False";
        }


        private string GetDBStringWithDB(string svr, string us, string pws,string db)
        {

            return "Server=" + svr + ";Database="+ db + ";User ID=" + us + ";Password=" + pws + ";Trusted_Connection=False";
        }


    }
}
