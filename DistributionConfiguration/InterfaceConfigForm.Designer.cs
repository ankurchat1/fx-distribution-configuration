﻿namespace DistributionConfiguration
{
    partial class InterfaceConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnChannalSave = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAPIUrl = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAuthKey = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAPIPassword = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAPIUserName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPropertyId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPmsCustCode = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbCommunicationType = new System.Windows.Forms.ComboBox();
            this.cmbDestinationIntetface = new System.Windows.Forms.ComboBox();
            this.cmbSourceInterface = new System.Windows.Forms.ComboBox();
            this.cmbMessageType = new System.Windows.Forms.ComboBox();
            this.cmbIFSCCode = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(349, 462);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(101, 35);
            this.btnCancel.TabIndex = 36;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnChannalSave
            // 
            this.btnChannalSave.Location = new System.Drawing.Point(215, 462);
            this.btnChannalSave.Name = "btnChannalSave";
            this.btnChannalSave.Size = new System.Drawing.Size(101, 35);
            this.btnChannalSave.TabIndex = 35;
            this.btnChannalSave.Text = "Save";
            this.btnChannalSave.UseVisualStyleBackColor = true;
            this.btnChannalSave.Click += new System.EventHandler(this.btnChannalSave_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(30, 424);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 15);
            this.label8.TabIndex = 34;
            this.label8.Text = "API Url";
            // 
            // txtAPIUrl
            // 
            this.txtAPIUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAPIUrl.Location = new System.Drawing.Point(215, 421);
            this.txtAPIUrl.Name = "txtAPIUrl";
            this.txtAPIUrl.Size = new System.Drawing.Size(272, 21);
            this.txtAPIUrl.TabIndex = 33;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(30, 381);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 15);
            this.label7.TabIndex = 32;
            this.label7.Text = "Authorization Key";
            // 
            // txtAuthKey
            // 
            this.txtAuthKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAuthKey.Location = new System.Drawing.Point(215, 378);
            this.txtAuthKey.Name = "txtAuthKey";
            this.txtAuthKey.Size = new System.Drawing.Size(272, 21);
            this.txtAuthKey.TabIndex = 31;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(30, 340);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 15);
            this.label6.TabIndex = 30;
            this.label6.Text = "API Password";
            // 
            // txtAPIPassword
            // 
            this.txtAPIPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAPIPassword.Location = new System.Drawing.Point(215, 337);
            this.txtAPIPassword.Name = "txtAPIPassword";
            this.txtAPIPassword.Size = new System.Drawing.Size(272, 21);
            this.txtAPIPassword.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(30, 296);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 15);
            this.label5.TabIndex = 28;
            this.label5.Text = "API User Name";
            // 
            // txtAPIUserName
            // 
            this.txtAPIUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAPIUserName.Location = new System.Drawing.Point(215, 293);
            this.txtAPIUserName.Name = "txtAPIUserName";
            this.txtAPIUserName.Size = new System.Drawing.Size(272, 21);
            this.txtAPIUserName.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(30, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 15);
            this.label4.TabIndex = 26;
            this.label4.Text = "Message Type";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(30, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 15);
            this.label3.TabIndex = 24;
            this.label3.Text = "Interface Code";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 22;
            this.label1.Text = "Property ID";
            // 
            // txtPropertyId
            // 
            this.txtPropertyId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPropertyId.Location = new System.Drawing.Point(215, 56);
            this.txtPropertyId.Name = "txtPropertyId";
            this.txtPropertyId.Size = new System.Drawing.Size(272, 21);
            this.txtPropertyId.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(30, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 15);
            this.label2.TabIndex = 20;
            this.label2.Text = "PMS Code";
            // 
            // txtPmsCustCode
            // 
            this.txtPmsCustCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPmsCustCode.Location = new System.Drawing.Point(215, 19);
            this.txtPmsCustCode.Name = "txtPmsCustCode";
            this.txtPmsCustCode.Size = new System.Drawing.Size(272, 21);
            this.txtPmsCustCode.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(30, 220);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(119, 15);
            this.label9.TabIndex = 40;
            this.label9.Text = "Destination Interface";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(30, 176);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 15);
            this.label10.TabIndex = 38;
            this.label10.Text = "Source Interface";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(30, 261);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(116, 15);
            this.label11.TabIndex = 42;
            this.label11.Text = "Commnication Type";
            // 
            // cmbCommunicationType
            // 
            this.cmbCommunicationType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCommunicationType.FormattingEnabled = true;
            this.cmbCommunicationType.Location = new System.Drawing.Point(215, 258);
            this.cmbCommunicationType.Name = "cmbCommunicationType";
            this.cmbCommunicationType.Size = new System.Drawing.Size(272, 23);
            this.cmbCommunicationType.TabIndex = 43;
            // 
            // cmbDestinationIntetface
            // 
            this.cmbDestinationIntetface.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDestinationIntetface.FormattingEnabled = true;
            this.cmbDestinationIntetface.Location = new System.Drawing.Point(215, 217);
            this.cmbDestinationIntetface.Name = "cmbDestinationIntetface";
            this.cmbDestinationIntetface.Size = new System.Drawing.Size(272, 23);
            this.cmbDestinationIntetface.TabIndex = 44;
            // 
            // cmbSourceInterface
            // 
            this.cmbSourceInterface.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSourceInterface.FormattingEnabled = true;
            this.cmbSourceInterface.Location = new System.Drawing.Point(215, 176);
            this.cmbSourceInterface.Name = "cmbSourceInterface";
            this.cmbSourceInterface.Size = new System.Drawing.Size(272, 23);
            this.cmbSourceInterface.TabIndex = 45;
            // 
            // cmbMessageType
            // 
            this.cmbMessageType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMessageType.FormattingEnabled = true;
            this.cmbMessageType.Location = new System.Drawing.Point(215, 138);
            this.cmbMessageType.Name = "cmbMessageType";
            this.cmbMessageType.Size = new System.Drawing.Size(272, 23);
            this.cmbMessageType.TabIndex = 46;
            // 
            // cmbIFSCCode
            // 
            this.cmbIFSCCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbIFSCCode.FormattingEnabled = true;
            this.cmbIFSCCode.Location = new System.Drawing.Point(215, 94);
            this.cmbIFSCCode.Name = "cmbIFSCCode";
            this.cmbIFSCCode.Size = new System.Drawing.Size(272, 23);
            this.cmbIFSCCode.TabIndex = 47;
            // 
            // InterfaceConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 509);
            this.Controls.Add(this.cmbIFSCCode);
            this.Controls.Add(this.cmbMessageType);
            this.Controls.Add(this.cmbSourceInterface);
            this.Controls.Add(this.cmbDestinationIntetface);
            this.Controls.Add(this.cmbCommunicationType);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnChannalSave);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtAPIUrl);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtAuthKey);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtAPIPassword);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAPIUserName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPropertyId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPmsCustCode);
            this.Name = "InterfaceConfigForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InterfaceConfigForm";
            this.Load += new System.EventHandler(this.InterfaceConfigForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnChannalSave;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtAPIUrl;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAuthKey;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAPIPassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAPIUserName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPropertyId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPmsCustCode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbCommunicationType;
        private System.Windows.Forms.ComboBox cmbDestinationIntetface;
        private System.Windows.Forms.ComboBox cmbSourceInterface;
        private System.Windows.Forms.ComboBox cmbMessageType;
        private System.Windows.Forms.ComboBox cmbIFSCCode;
    }
}